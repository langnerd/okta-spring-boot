package com.langnerd.okta

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class OktaHostedLoginApplication

fun main(args: Array<String>) {
    runApplication<OktaHostedLoginApplication>(*args)
}
