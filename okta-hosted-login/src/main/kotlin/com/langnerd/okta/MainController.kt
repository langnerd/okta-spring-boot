package com.langnerd.okta

import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.reactive.result.view.Rendering
import reactor.core.publisher.Mono

@Controller
class MainController {

    @GetMapping("/")
    fun home(): Mono<String> = Mono.just("home")

    @GetMapping("/profile")
    @PreAuthorize("hasAuthority('SCOPE_profile')")
    fun userDetails(authentication: OAuth2AuthenticationToken): Mono<Rendering> =
        Mono.just(
            Rendering.view("userProfile")
                .model(mapOf("details" to authentication.principal.attributes)).build()
        )
}